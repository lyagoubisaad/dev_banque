package com.example.banque.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

public class UserControllerTest {
	  @Autowired
	  private MockMvc mockMvc;
	
	 @Test
	  public void givenPathWithoutDotShouldReturnString() throws Exception {
	    this.mockMvc.perform(get("/add"))
	        .andExpect(forwardedUrl("/users/form"));
	  }
}
