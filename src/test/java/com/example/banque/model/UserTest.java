package com.example.banque.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UserTest {

	@Test
	void testUserGetName() {
		String userName = "foo";
		User user = new User();
		user.setName(userName);
		assertEquals(user.getName(), userName);
	}
	
	@Test
	void testUserNameConstructor() {
		String userName = "foo";
		User user = new User(userName);
		assertEquals(user.getName(), userName);
	}
	
	@Test
	void testUserSetName() {
		String userName = "foo";
		User user = new User("bar");
		user.setName(userName);
		assertEquals(user.getName(), userName);
	}
	
	@Test
	void testUserGeId() {
		long id = 16414113;
		User user = new User();
		user.setId(id);
		assertEquals(user.getId(), id);
	}

}
