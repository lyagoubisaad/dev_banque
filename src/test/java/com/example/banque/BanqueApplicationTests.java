package com.example.banque;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.assertEquals;



@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class BanqueApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void homeResponse() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertEquals("Spring is here!!", body);
	}
	
	@Test
	public void healthResponse() {
		String body = this.restTemplate.getForObject("/health", String.class);
		assertEquals("ok", body);
	}

	@Test
	public void contextTest() {
		BanqueApplication.main(new String[] {});
	}
	


}
