package com.example.banque.model;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
    /**
     * the id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
    private Long id;
    @OneToMany()
    private Set<Compte> comptes;

    /**
     * the name.
     */
    private String name;
    private String password;
    /**
     * the user.
     */
    public User() {
    }

    /**
     *
     * this user contructor.
     * @param userName the name of the user.
     */
    public User(final String userName) {
        this.name = userName;
    }

    /**
     * return the id.
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * set the id.
     * @param userId to set
     */
    public void setId(final Long userId) {
        this.id = userId;
    }

    /**
     * return the name of the user.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * set the name of the user.
     * @param userName
     */
    public void setName(final String userName) {
        this.name = userName;
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(final String userPassword) {
		this.password = userPassword;
	}
	public void retraitCompte (Compte compte) {
		this.comptes.remove(compte);
	}
}
