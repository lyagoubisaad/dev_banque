package com.example.banque.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "compte")
public class Compte {
    /**
     * the id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    /**
     * the solde.
     */
    private float solde;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    
    /**
     * the compte
     */
    public Compte() {
    }

    /**
     *
     * this user contructor.
     * @param solde the name of the compte.
     */
    public Compte(final float solde , final User user) {
        this.solde = solde;
        this.user = user;
    }

    /**
     * return the id.
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * set the id.
     * @param userId to set
     */
    public void setId(final long userId) {
        this.id = userId;
    }

    /**
     * return the solde of the compte.
     * @return solde
     */
    public float getSolde() {
        return solde;
    }

    /**
     * set the solde of the compte.
     * @param compteSolde
     */
    public void setSolde(final float compteSolde) {
        this.solde = compteSolde;
    }
    public void retrait(final float solde) {
    	this.solde -= solde;
    }
    public void depot(final float solde) {
    	this.solde += solde;
    }
    public Long getUserId () {
    	return this.user.getId();
    }
}
