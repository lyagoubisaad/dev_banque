package com.example.banque.controllers;

import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.banque.model.User;
import com.example.banque.model.Compte;
import com.example.banque.repository.UserRepository;
import com.example.banque.repository.CompteRepository;
@Controller
@RequestMapping(path = "/user")
public class UserController {

    /**
     * userRepository.
     */
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CompteRepository compteRepository;

    /**
     * add a user.
     * @param model the model.
     * @return user.html contents
     */
    @GetMapping("/add")
    public String addUser(final Model model) {
        model.addAttribute("user", new User());
        return "users/form";
    }

    /**
     * add a user.
     * @param user the user to add.
     * @param bindingResult the binding result
     * @return user/user or user/all
     */
    @PostMapping("/add")
    public String addUser(@Valid final User user,
            final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "users/form";
        }
        userRepository.save(user);
        return "redirect:/user/all";

    }
    /**
     * list all the users.
     * @param model the model
     * @return user/all
     */
    @GetMapping("/all")
    public String users(final Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "users/all";
    }
	  @RequestMapping(value = "/detail", method = RequestMethod.GET)
	  public String show( @RequestParam Long id,Model model) {
			Optional<User> userOp = userRepository.findById(id);
	        Set<Compte> comptes = compteRepository.findByUser(userOp);
	        if(userOp.isPresent()) 
	        {
	        	User user = userOp.get();
	        	model.addAttribute("user", user);
	        	model.addAttribute("comptes", comptes);
	        	return "users/show";
	        }else {
	        	return "users/all";
		}
	  }


}
