package com.example.banque.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.banque.model.Compte;
import com.example.banque.model.User;
import com.example.banque.repository.CompteRepository;
import com.example.banque.repository.UserRepository;
@Controller
@RequestMapping(path = "/compte")
public class CompteController {
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private UserRepository userRepository;
    @GetMapping("/add")
    public String addCompte(final Model model , @RequestParam Long id) {
        model.addAttribute("compte", new Compte());
		Optional<User> userOp = userRepository.findById(id);
        model.addAttribute("user", userOp);
        return "comptes/form";
    }

    /**
     * add a user.
     * @param user the user to add.
     * @param bindingResult the binding result
     * @return user/user or user/all
     */
	  @RequestMapping(value = "/add", method = RequestMethod.POST)
	  public String addCompte(@RequestParam Long id, @RequestParam float solde) {
			Optional<User> userOp = userRepository.findById(id);
			compteRepository.save(new Compte(solde, userOp.get()));
			return "redirect:/user/detail?id="+userOp.get().getId();
	    }
		@RequestMapping(value = "/close", method = RequestMethod.POST)
		public String fermerCompte (@RequestParam Long id_compte) {
			Optional<Compte> compte = compteRepository.findById(id_compte);
			Optional<User> userOp = userRepository.findById(compte.get().getUserId());
			userOp.get().retraitCompte(compte.get());
			compteRepository.delete(compte.get());
			return "redirect:/user/detail?id="+userOp.get().getId();
		}
	    @RequestMapping(value = "/retrait", method = RequestMethod.POST)
	    public String retraitCompte(@RequestParam Long id_compte,  @RequestParam float solde) {
			Optional<Compte> compte = compteRepository.findById(id_compte);
			compte.get().retrait(solde);
			compteRepository.save(compte.get());
			Long id_user = compte.get().getUserId();
			return "redirect:/user/detail?id="+id_user;
	    }
	    @RequestMapping(value = "/retrait", method = RequestMethod.GET)
	    public String retraitCompte(final Model model , @RequestParam Long id_compte) {	       
	        model.addAttribute("compte", compteRepository.findById(id_compte));
	        return "comptes/retrait";
	    }
	    @RequestMapping(value = "/depot", method = RequestMethod.POST)
	    public String depotCompte(@RequestParam Long id_compte,  @RequestParam float solde) {
			Optional<Compte> compte = compteRepository.findById(id_compte);
			compte.get().depot(solde);
			compteRepository.save(compte.get());
			Long id_user = compte.get().getUserId();
			return "redirect:/user/detail?id="+id_user;
	    }
	    @RequestMapping(value = "/depot", method = RequestMethod.GET)
	    public String depotCompte(final Model model , @RequestParam Long id_compte) {	       
	        model.addAttribute("compte", compteRepository.findById(id_compte));
	        return "comptes/depot";
	    }
	    @RequestMapping(value = "/transfert", method = RequestMethod.POST)
	    public String virementCompte(@RequestParam Long id_compte1,@RequestParam Long id_compte2,  @RequestParam float montant) {
			Optional<Compte> from = compteRepository.findById(id_compte1);
			Optional<Compte> to = compteRepository.findById(id_compte2);
			from.get().retrait(montant);
			compteRepository.save(from.get());
			to.get().depot(montant);
			compteRepository.save(to.get());
			Long id_user = from.get().getUserId();
			return "redirect:/user/detail?id="+id_user;
	    }
	    @RequestMapping(value = "/transfert", method = RequestMethod.GET)
	    public String virementCompte(final Model model , @RequestParam Long id_compte) {	
	        model.addAttribute("compte", compteRepository.findById(id_compte));
	        return "comptes/virement";
	    }

}
