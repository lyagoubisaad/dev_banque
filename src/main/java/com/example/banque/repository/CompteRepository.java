package com.example.banque.repository;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.example.banque.model.Compte;
import com.example.banque.model.User;
public interface CompteRepository extends CrudRepository<Compte , Long> {
    Set<Compte> findByUser(Optional<User> userOp);
}
