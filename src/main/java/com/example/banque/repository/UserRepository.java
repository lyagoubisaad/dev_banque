package com.example.banque.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.banque.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	 Optional<User> findById(Long id);
}
