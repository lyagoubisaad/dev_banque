package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

    /**
     * @return This is javadoc.
     */
    @GetMapping("/")
    String home() {
        return "Spring is here!!";
    }

    /**
     * returns ok in order to inform the infrastructure that it is alive.
     * @return string ok.
     */
    @GetMapping("/health")
    String health() {
        return "ok";
    }

    /**
     * @param args : This is javadoc.
     */
    public static void main(final String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}

